resource "azurerm_resource_group" "azurerm_resource_group_1" {
  tags     = merge(var.tags, {})
  location = "eastus"
  name = "myRessourceGroup"
}

resource "azurerm_subnet" "azurerm_subnet_2" {
  virtual_network_name = azurerm_virtual_network.azurerm_virtual_network_3.name
  resource_group_name  = azurerm_resource_group.azurerm_resource_group_1.name
  name                 = "mySubNetwork"

  address_prefixes = [
    "10.0.1.0/24",
  ]
}

resource "azurerm_virtual_network" "azurerm_virtual_network_3" {
  tags                = merge(var.tags, {})
  resource_group_name = azurerm_resource_group.azurerm_resource_group_1.name
  name                = "myNetwork"
  location            = azurerm_resource_group.azurerm_resource_group_1.location

  address_space = [
    "10.0.0.0/16",
  ]
}

resource "azurerm_public_ip" "azurerm_public_ip_4" {
  tags                = merge(var.tags, {})
  resource_group_name = azurerm_resource_group.azurerm_resource_group_1.name
  name                = "myPublicIP"
  location            = azurerm_resource_group.azurerm_resource_group_1.location
  allocation_method   = "Dynamic"
}

resource "azurerm_network_security_group" "azurerm_network_security_group_5" {
  resource_group_name = azurerm_resource_group.azurerm_resource_group_1.name
  name                = "mySecurityGroup"
  location = azurerm_resource_group.azurerm_resource_group_1.location

  security_rule {
    source_port_range          = "*"
    source_address_prefix      = "*"
    protocol                   = "Tcp"
    priority                   = 1001
    name                       = "SSH"
    direction                  = "Inbound"
    destination_port_range     = "22"
    destination_address_prefix = "*"
    access                     = "Allow"
  }
    security_rule {
    source_port_range          = "*"
    source_address_prefix      = "*"
    protocol                   = "*"
    priority                   = 1020
    name                       = "Graphana"
    direction                  = "Inbound"
    destination_port_range     = "3000"
    destination_address_prefix = "*"
    access                     = "Allow"
  }
}

resource "azurerm_network_interface" "azurerm_network_interface_6" {
  tags                = merge(var.tags, {})
  resource_group_name = azurerm_resource_group.azurerm_resource_group_1.name
  name                = "myNetworkInterface"
  location            = azurerm_resource_group.azurerm_resource_group_1.location

  ip_configuration {
    subnet_id                     = azurerm_subnet.azurerm_subnet_2.id
    public_ip_address_id          = azurerm_public_ip.azurerm_public_ip_4.id
    private_ip_address_allocation = "Dynamic"
    name                          = "myIpNeworkInterfaceConf"
  }
}

resource "azurerm_network_interface_security_group_association" "azurerm_network_interface_application_security_group_association_7" {
  network_interface_id          = azurerm_network_interface.azurerm_network_interface_6.id
  network_security_group_id = azurerm_network_security_group.azurerm_network_security_group_5.id
}

resource "azurerm_linux_virtual_machine" "azurerm_linux_virtual_machine_8" {
  tags                            = merge(var.tags, {})
  size                            = "Standard_DS1_v2"
  resource_group_name             = azurerm_resource_group.azurerm_resource_group_1.name
  disable_password_authentication = true
  computer_name                   = "supervm"
  admin_username                  = "etumiage"
  location                        = azurerm_resource_group.azurerm_resource_group_1.location
  name = "myLinuxVM"


  admin_ssh_key {
    username   = "etumiage"
    public_key = var.ssh_public_key
  }

  network_interface_ids = [
    azurerm_network_interface.azurerm_network_interface_6.id,
  ]

  os_disk {
    storage_account_type = "Premium_LRS"
    name                 = "mydisk1"
    caching              = "ReadWrite"
  }

  source_image_reference {
    version   = "latest"
    sku       = "22_04-lts-gen2"
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
  }
}

