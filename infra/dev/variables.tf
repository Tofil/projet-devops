variable "ssh_public_key" {
  type = string
}

variable "tags" {
  description = "Default tags to apply to all resources."
  type        = map(any)
}

